package com.example.imagespickmodule;

import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;

import java.util.ArrayList;

/**
 * Created by Sayed on 8/18/2017.
 */

public class AndroidMultiImagePicker extends BaseAndroidImagePicker {
    IMultiImagePickerCallback callback;

    public AndroidMultiImagePicker(IImagePickerHandler handler, IMultiImagePickerCallback callback) {
        super(handler);
        this.callback = callback;
    }

    @Override
    public void openPicker() {
        Intent intent = getIntent();
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        imagePickerHandler.startPickerHandler(intent, IMAGE_PICKER_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            ArrayList<Uri> resultUri = new ArrayList<Uri>();
            if (data.getClipData() != null) {
                ClipData mClipData = data.getClipData();

                for (int i = 0; i < mClipData.getItemCount(); i++) {

                    ClipData.Item item = mClipData.getItemAt(i);
                    Uri uri = item.getUri();
                    resultUri.add(uri);

                }
            }
            if (resultUri.size() > 0)
                callback.onUrisReceived(resultUri);
            else
                callback.onNoThingSelected();
        }
    }
}

