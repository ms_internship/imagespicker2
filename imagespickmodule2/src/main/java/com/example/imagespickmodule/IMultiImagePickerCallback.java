package com.example.imagespickmodule;

import android.net.Uri;

import java.util.List;

/**
 * Created by Sayed on 8/18/2017.
 */

public interface IMultiImagePickerCallback {

    void onUrisReceived(List<Uri> resultUri);

    void onNoThingSelected();
}
