package com.example.imagespickmodule;

import android.content.Intent;
import android.net.Uri;

/**
 * Created by Sayed on 8/17/2017.
 */

public class AndroidSingleImagePicker extends BaseAndroidImagePicker {
    ISingleImagePickerCallback callback;
    public AndroidSingleImagePicker( IImagePickerHandler handler, ISingleImagePickerCallback callback) {
        super( handler );
        this.callback = callback;
    }
    @Override
    public void openPicker() {
        imagePickerHandler.startPickerHandler(getIntent() , IMAGE_PICKER_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && data != null) {

             if (data.getData() != null) {

                Uri imageUri = data.getData();
                callback.onUriReceived(imageUri);

            }else
                callback.onNoThingSelected();
        }
    }


}
