package com.example.imagespickmodule;

import android.net.Uri;

/**
 * Created by Sayed on 8/16/2017.
 */

public interface ISingleImagePickerCallback {

   void onUriReceived(Uri resultUri);

   void onNoThingSelected();

}
