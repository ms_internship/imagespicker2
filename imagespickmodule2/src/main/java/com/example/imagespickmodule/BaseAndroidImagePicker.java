package com.example.imagespickmodule;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by Sayed on 8/17/2017.
 */

public abstract class BaseAndroidImagePicker implements IImagePicker {
    IImagePickerHandler imagePickerHandler;
    ISingleImagePickerCallback callback;
    Intent intent;

     final static int IMAGE_PICKER_REQUEST_CODE = 1;
     final static int RESULT_OK = Activity.RESULT_OK;

    public BaseAndroidImagePicker(IImagePickerHandler handler) {
        intent = new Intent();
        imagePickerHandler = handler;

    }

    public Intent getIntent() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        return intent;
    }
   public abstract void  onActivityResult(int requestCode, int resultCode, Intent data);

}
