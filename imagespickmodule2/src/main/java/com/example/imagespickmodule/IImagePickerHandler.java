package com.example.imagespickmodule;

import android.content.Intent;

/**
 * Created by Sayed on 8/17/2017.
 */

public interface IImagePickerHandler {

    void startPickerHandler(Intent data, int requestCode);

}
