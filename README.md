
### What is this repository for? ###

This is imagepicker library, can be used to select single ot multiple images.
V 1.0

### How do I get set up? ###

till now you can clone the project and import it from . File -> New -> Import Module ..
then select the module path.
in the Cloned project select imagespickmodule2 .. this is the module path.


## Usage 


    example for the single Picker
    public class myActivity extends AppCompatActivity implements ISingleImagePickerCallback , IImagePickerHandler {
    BaseAndroidImagePicker imagePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        imagePicker = new AndroidSingleImagePicker(this , this); // first parameter the handler, secone the call back that you have to implment
         // imagePicker = new AndroidMultiImagePicker(this , this); for multiple
   }
    
    
	// here you delgate the onActivityResult
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePicker.onActivityResult(requestCode , resultCode , data);
    }


    @Override
    public void onUriReceived(Uri resultUri) {
        // here you have the uri of the selected image, do whatever you want.


    }

    @Override
    public void onNoThingSelected() {
        // handle if the user didn't select anything.
    }

    @Override
    public void startPickerHandler(Intent data, int requestCode) {
        startActivityForResult(Intent.createChooser(data,
                "Select Picture"), requestCode); // this way you start the picker with the given intent.

    }
	
	    // to start the picker you have to call openPicker from imagePicker instance you have

    }



## Class Diagram 
https://ibb.co/bKfR1k 
